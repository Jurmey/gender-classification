import tensorflow as tf
from flask import Flask, render_template, request
from keras.models import load_model
from keras.preprocessing import image
import numpy as np

app = Flask(__name__)

model = load_model('gender_model.h5')

model.make_predict_function()

def predict_label(img_path):
	i = image.load_img(img_path, target_size=(224,224))
	i = image.img_to_array(i)
	i = i/225.0
	i = i.reshape(1,224,224,3)
	p = model.predict(i) #predict the class 

	ind = np.where(p[0] == np.amax(p[0]))
	i = ind[0][0]

	dic = {0 : 'Female', 1 : 'Male', 2 : 'Unknown'}
	return dic[i]


# routes
@app.route("/", methods=['GET', 'POST'])
def main():
	return render_template("index.html")

@app.route("/submit", methods = ['GET', 'POST'])
def get_output():

	if request.method == 'POST': 
		img = request.files['my_image']

		img_path = "static/" + img.filename	
		img.save(img_path)

		p = predict_label(img_path)

	return render_template("index.html", prediction = p, img_path = img_path)

@app.route("/remove", methods=['GET', 'POST'])
def remove():
	return render_template("index.html")

if __name__ =='__main__':
	#app.debug = True
	app.run(debug = True)



#dic = { 0 :'Black', 1 :'Blue', 2 :'Brown', 3 :'Green', 4 :'Grey', 5 :'Orange', 6 :'Red', 7 :'Violet', 8 :'White', 9 :'Yellow' }